// importing from npm packages
import {useContext,useState, useEffect} from "react";
import {/*Link,*/ NavLink, Outlet} from "react-router-dom";
import {Nav, Navbar} from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faGift, faShoppingCart, faUser, faUserPlus, faSignInAlt, faSignOutAlt, faChartBar } from '@fortawesome/free-solid-svg-icons';

// importing from within the app
import UserContext from "../UserContext";
import ShawpLogo from "../images/ShawpLogo.png";

export default function AppNavbar()
{
	const {user} = useContext(UserContext);
	const [isMobile, setIsMobile] = useState(false);

	// making different navbars render depending on screensize

	const SmallNavbar = () =>
	{
		return(
		  <Navbar bg="light" expand="md" className="p-3 mt-10 justify-content-center" fixed="bottom">
		    <Nav className="d-flex align-items-center flex-row">
		      <Nav.Link as={NavLink} to="/" className="p-2 text-center" active={window.location.pathname === '/'}>
		        <div className="d-flex flex-column">
		          <FontAwesomeIcon icon={faHome} size="lg" />
		          <div>Home</div>
		        </div>
		      </Nav.Link>

		      {(user.id !== null) ?
		        <>
		          {user.isAdmin ?
		            <Nav.Link as={NavLink} to="/dashboard" className="p-2 text-center" active={window.location.pathname === '/dashboard'}>
		              <div className="d-flex flex-column">
		                <FontAwesomeIcon icon={faChartBar} size="lg" />
		                <div>Dashboard</div>
		              </div>
		            </Nav.Link>
		            :
		            <>
		              <Nav.Link as={NavLink} to="/orders" className="p-2 text-center" active={window.location.pathname === '/orders'}>
		                <div className="d-flex flex-column">
		                  <FontAwesomeIcon icon={faGift} size="lg" />
		                  <div>Orders</div>
		                </div>
		              </Nav.Link>
		              <Nav.Link as={NavLink} to="/cart" className="p-2 text-center" active={window.location.pathname === '/cart'}>
		                <div className="d-flex flex-column">
		                  <FontAwesomeIcon icon={faShoppingCart} size="lg" />
		                  <div>Cart</div>
		                </div>
		              </Nav.Link>
		            </>
		          }
		          <Nav.Link as={NavLink} to="/profile" className="p-2 text-center" active={window.location.pathname === '/profile'}>
		            <div className="d-flex flex-column">
		              <FontAwesomeIcon icon={faUser} size="lg" />
		              <div>{user.username}</div>
		            </div>
		          </Nav.Link>
		          <Nav.Link as={NavLink} to="/logout" className="p-2 text-center">
		            <div className="d-flex flex-column">
		              <FontAwesomeIcon icon={faSignOutAlt} size="lg" />
		              <div>Log Out</div>
		            </div>
		          </Nav.Link>
		        </>
		        :
		        <>
		          <Nav.Link as={NavLink} to="/register" className="p-2 text-center">
		            <div className="d-flex flex-column">
		              <FontAwesomeIcon icon={faUserPlus} size="lg" />
		              <div>Sign Up</div>
		            </div>
		          </Nav.Link>
		          <Nav.Link as={NavLink} to="/login" className="p-2 text-center">
		            <div className="d-flex flex-column">
		              <FontAwesomeIcon icon={faSignInAlt} size="lg" />
		              <div>Log In</div>
		            </div>
		          </Nav.Link>
		        </>
		      }
		    </Nav>
		  </Navbar>
		)

	}

	const LargeNavbar = () =>
	{
		return (
		  <Navbar bg="light" expand="md" className="p-3 mt-5" fixed="bottom">
		    <Navbar.Brand as={NavLink} to="/"active={window.location.pathname === '/'}>
		      <img src={ShawpLogo} height="30" alt="Shawp Logo" className="d-inline-block align-top mr-2" />
		      <span className="ms-2">Shawp</span>
		    </Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav" className="mr-auto">
		      <Nav className="ms-auto">
		        <Nav.Link as={NavLink} to="/" className="p-2" active={window.location.pathname === '/'}>
		          <FontAwesomeIcon icon={faHome} size="lg" />
		          <span className="ms-2">Home</span>
		        </Nav.Link>

		        {(user.id !== null) ?
		          <>
		            {user.isAdmin ?
		              <Nav.Link as={NavLink} to="/dashboard" className="p-2" active={window.location.pathname === '/dashboard'}>
		                <FontAwesomeIcon icon={faChartBar} size="lg" />
		                <span className="ms-2">Dashboard</span>
		              </Nav.Link>
		              :
		              <>
		                <Nav.Link as={NavLink} to="/orders" className="p-2" active={window.location.pathname === '/orders'}>
		                  <FontAwesomeIcon icon={faGift} size="lg" />
		                  <span className="ms-2">Orders</span>
		                </Nav.Link>
		                <Nav.Link as={NavLink} to="/cart" className="p-2" active={window.location.pathname === '/cart'}>
		                  <FontAwesomeIcon icon={faShoppingCart} size="lg" />
		                  <span className="ms-2">Cart</span>
		                </Nav.Link>
		              </>
		            }
		            <Nav.Link as={NavLink} to="/profile" className="p-2" active={window.location.pathname === '/profile'}>
		              <FontAwesomeIcon icon={faUser} size="lg" />
		              <span className="ms-2">{user.username}</span>
		            </Nav.Link>
		            <Nav.Link as={NavLink} to="/logout" className="p-2">
		              <FontAwesomeIcon icon={faSignOutAlt} size="lg" />
		              <span className="ms-2">Log Out</span>
		            </Nav.Link>
		          </>
		          :
		          <>
		            <Nav.Link as={NavLink} to="/register" className="p-2">
		              <FontAwesomeIcon icon={faUserPlus} size="lg" />
		              <span className="ms-2">Sign Up</span>
		            </Nav.Link>
		            <Nav.Link as={NavLink} to="/login" className="p-2">
		              <FontAwesomeIcon icon={faSignInAlt} size="lg" />
		              <span className="ms-2">Log In</span>
		            </Nav.Link>
		          </>
		        }

		      </Nav>
		    </Navbar.Collapse>
		  </Navbar>
		);

	}

	// const [nav, setNav] = useState(SmallNavbar);
	// toggling visibility of each navbar between screen sizes

	/*{window.innerWidth < 576 ? <SmallNavbar /> : <LargeNavbar />}*/

	useEffect(() => {
	  const handleResize = () => {
	    setIsMobile(window.innerWidth < 768); // set isMobile based on screen width
	  };
	  handleResize(); // initial call to set isMobile on component mount
	  window.addEventListener('resize', handleResize); // add event listener for resize
	  return () => {
	    window.removeEventListener('resize', handleResize); // remove event listener on component unmount
	  };
	}, []);

	return(
		<>
    {isMobile ?
    <SmallNavbar />
    :
    <LargeNavbar />
  	}
  	<Outlet/>
    </>
	)
}