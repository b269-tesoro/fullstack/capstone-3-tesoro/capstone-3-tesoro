import { useState,useContext,useEffect } from "react";
import { Form, Button, Card } from "react-bootstrap";
import {/*Navigate,*/ useNavigate, useParams} from 'react-router-dom';

import Swal from 'sweetalert2';


import UserContext from "../UserContext"
export default function ProductUpdate() 
{
  const [image, setImage] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [category, setCategory] = useState("");
  const [salePercent, setSalePercent] = useState("");
  const [stock, setStock] = useState("");
  // const [product, setProduct] = useState({});

  const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const {productId} = useParams();

  function UpdateProduct(e)
  {
    e.preventDefault();

    //since the function takes the string value of the category and turns it into array, it's important to put conditionals for in case the admin does not update category. Without this, there will be an error due to trying to split an array instead of a string.
    let arrayCategory = "";
    if(Array.isArray(category))
    {
    	arrayCategory = category;
    }
    else
    {
    	arrayCategory = category.split(/\s*,\s*/);
    }

    console.log(arrayCategory);
    if(user.isAdmin)
    {
  	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`,
  	{
  		method: "PUT",
  		headers:
  		      {
  		      	"Content-Type" : "application/json",
  		        Authorization: `Bearer ${localStorage.getItem("token")}`
  		      },
  		body: JSON.stringify(
  					{
  						image: image,
  						name: name,
				      description: description,
				      price: price,
				      category: arrayCategory,
				      salePercent: salePercent,
				      stock: stock
  					}) 
  	}).then(res =>res.json()).then(data =>
		{
			// console.log(data)
			if (data.error)
			{
				Swal.fire({
					title: `${data.error}`,
                  icon: "error",
                  // text: "Please provide a different email."
				})
			}
			else
			{
				Swal.fire({
                  title: `${data.success}`,
                  icon: "success",
                  // text: "Please proceed to login."
              	})
				
      	navigate(`/products/${productId}`);
			}
		})
  }
	};

	// Fetching current product details to set getters:
	useEffect(()=>
	{
		fetch(`${process.env.REACT_APP_API_URL}/products/product/${productId}`)
		      .then((res) => res.json())
		      .then((data) => {

		        // console.log(data);

		        if(data.image)
		        {
		        	setImage(data.image);
		        }
		        setName(data.name);
		        setDescription(data.description);
		        setPrice(data.price);
		        setCategory(data.category);
		        setSalePercent(data.salePercent);
		        setStock(data.stock);

		        // setProduct(data);
		      });
	},[productId])

  return (
    <div className="d-flex justify-content-center mt-5">
      <Card style={{ width: "25rem" }}>
        <Card.Body>
          <Card.Title>Update Product</Card.Title>
          <Form onSubmit={(e) => UpdateProduct(e)}>

          	<Form.Group controlId="formProductImage">
          	  <Form.Label>Product Image</Form.Label>
          	  <Form.Control
          	    type="text"
          	    placeholder="Enter product image url"
          	    value={image}
          	    onChange={(e) => setImage(e.target.value)}
          	    required
          	  />
          	</Form.Group>

            <Form.Group controlId="formProductName">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product name"
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="formProductDescription">
              <Form.Label>Product Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                placeholder="Enter product description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="formProductPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="formProductCategory">
              <Form.Label>Category</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter categories separated by commas"
                value={category}
                onChange={(e) => setCategory(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="formProductSalePercent">
              <Form.Label>Sale Percent</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter sale percent"
                value={salePercent}
                onChange={(e) => setSalePercent(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="formProductStock">
              <Form.Label>Stock</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter stock"
                value={stock}
                onChange={(e) => setStock(e.target.value)}
                required
              />
            </Form.Group>

            <Button variant="primary" type="submit">
              Update Product
            </Button>

          </Form>
        </Card.Body>
      </Card>
    </div>
  );
}