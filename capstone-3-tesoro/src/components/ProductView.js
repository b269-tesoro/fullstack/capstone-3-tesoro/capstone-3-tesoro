// importing from npm packages
import { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Card, Button, ButtonGroup, Form } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar as faStarSolid, faStar as faStarRegular } from '@fortawesome/free-solid-svg-icons';
import Swal from 'sweetalert2';

// importing from within the app
import UserContext from '../UserContext';

export default function ProductView() {
  const { user } = useContext(UserContext);
  const { productId } = useParams();
  const navigate = useNavigate();

  const [quantity, setQuantity] = useState(1);
  const [product, setProduct] = useState({});

  const [isBought, setIsBought] = useState(false);
  // const [buyerReview, setBuyerReview] = useState(null);
  const [buyerOrder, setBuyerOrder] = useState("");
  const [isAnon, setIsAnon] = useState(false);
  const [rating, setRating] = useState(0);
  const [review, setReview] = useState("");

  // handling review
  function handleReview(e) 
  {
    e.preventDefault();

    // console.log(buyerOrder);
    // console.log(rating);
    // console.log(review);
    // console.log(isAnon);
    // console.log(productId);

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/review`,
    {
          method: "PUT",
          headers:
          {
            "Content-Type" : "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`
          },
          body: JSON.stringify(
          {
            orderId: buyerOrder,
            userRating: rating,
            userReview: review,
            isAnon: isAnon
          })
        }).then(res =>{console.log(res); res.json()}).then(data =>
        {
          // console.log(data)
          // if (data.error)
          // {
          //   Swal.fire({
          //     title: `${data.error}`,
          //             icon: "error",
          //             // text: "Please provide a different email."
          //   })
          // }
          // else
          // {
            Swal.fire({
                      title: `Reviewed Successfully!`,
                      icon: "success",
                      // text: "Please proceed to login."
                    })

            navigate("/orders");
            
          // }
        })
  }

  const handleQuantityChange = (value) => 
  {
    if (value >= 1 && value <= product.stock) {
      setQuantity(value);
    }
  };

  const handleAddToCart = (e) => 
  {
    console.log(user)
    fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/cart/add`,
    {
      method: "PUT",
      headers:
      {
        "Content-Type" : "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify(
      {
        productId: productId,
        quantity: quantity
      })
    }).then(res =>res.json()).then(data =>
        {
          // console.log(data)
          if (data.error)
          {
            Swal.fire({
              title: `${data.error}`,
                      icon: "error",
                      // text: "Please provide a different email."
            })
          }
          else
          {
            Swal.fire({
                      title: `${data.success}`,
                      icon: "success",
                      // text: "Please proceed to login."
                    })
            
             navigate(`/products`);
          }
        })
  };


  const handleBuyNow = () => 
  {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/buynow`,
      {
        method: "POST",
        headers:
        {
          "Content-Type" : "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`
        },
        body: JSON.stringify(
        {
          quantity: quantity
        })
      }).then(res =>res.json()).then(data =>
        {
          console.log(data)
          if (data.error)
          {
            Swal.fire({
              title: `${data.error}`,
                      icon: "error",
                      // text: "Please provide a different email."
            })
          }
          else
          {
            Swal.fire({
                      title: `${data.success}`,
                      icon: "success",
                      // text: "Please proceed to login."
                    })
            
            navigate(`/orders`);
          }
        })
  };

  // admin only product deactivation
  function handleDeactivate(e)
  {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,
        {
          method: "PATCH",
          headers:
                {
                  "Content-Type" : "application/json",
                  Authorization: `Bearer ${localStorage.getItem("token")}`
                } 
        }).then(res =>res.json()).then(data =>
        {
          // console.log(data)
          if (data.error)
          {
            Swal.fire({
              title: `${data.error}`,
                      icon: "error",
                      // text: "Please provide a different email."
            })
          }
          else
          {
            Swal.fire({
                      title: `${data.success}`,
                      icon: "success",
                      // text: "Please proceed to login."
                    })
            
            navigate(`/products`);
          }
        })
  }

  // admin only function for restoring archived products
  function handleRestore(e)
  {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/restore`,
      {
        method: "PATCH",
        headers:
              {
                "Content-Type" : "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
              } 
      }).then(res =>res.json()).then(data =>
      {
        // console.log(data)
        if (data.error)
        {
          Swal.fire({
            title: `${data.error}`,
                    icon: "error",
                    // text: "Please provide a different email."
          })
        }
        else
        {
          Swal.fire({
                    title: `${data.success}`,
                    icon: "success",
                    // text: "Please proceed to login."
                  })
          
          navigate(`/products`);
        }
      })
  }

  // for rendering FeedbackCard
  const FeedbackCard = () => {
    if (product.userFeedback && product.userFeedback.length > 0) {
      return (
        <Card className="mt-4">
          <Card.Header>User Feedback</Card.Header>
          <Card.Body>
            {product.userFeedback.map((feedback) => (
              <div key={feedback._id} className="mb-3">
                <div className="d-flex align-items-center mb-2">
                  <div className="mr-2">{feedback.reviewName}</div>
                  <div className="d-flex">
                    {[...Array(feedback.userRating)].map((_, index) => (
                      <FontAwesomeIcon
                        key={index}
                        icon={faStarSolid}
                        className="mr-1"
                        style={{ color: 'gold' }}
                      />
                    ))}
                    {[...Array(5 - feedback.userRating)].map((_, index) => (
                      <FontAwesomeIcon
                        key={index}
                        icon={faStarRegular}
                        className="mr-1"
                        style={{ color: '#d3d3d3' }}
                      />
                    ))}
                  </div>
                </div>
                <div>{feedback.userReview}</div>
              </div>
            ))}
          </Card.Body>
        </Card>
      );
    }
    return null;
  };

  useEffect(() => 
  {
    fetch(`${process.env.REACT_APP_API_URL}/products/product/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProduct(data);
      });

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/checkuser`,
    {
      method: "POST",
      headers:
      {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then(res => res.json()).then(data=>
    {
      if(!data.exists)
      {
        setIsBought(false)
      }
      if(data.exists === true)
      {
        setIsBought(true);
        setBuyerOrder(data.orderId)
        /*setBuyerReview(
          <Card>
            <Form onSubmit={handleReview}>
              <Form.Group controlId={"starRating"}>
                <Form.Label>Star Rating</Form.Label>
                <div>
                  {Array.from({ length: 5 }, (_, i) => (
                    <FontAwesomeIcon
                      key={i}
                      icon={i < rating ? faStarSolid : faStarRegular}
                      onClick={() => {
                          setRating(i + 1);
                        }}

                      style={{ 
                        cursor: "pointer",
                        color: i < rating ? "gold" : "gray" 
                      }}
                    />
                  ))}
                </div>
              </Form.Group>
              <Form.Group controlId="review">
                <Form.Label>Review</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  value={review}
                  onChange={(e) => setReview(e.target.value)}
                />
              </Form.Group>
              <Form.Group controlId="anonymous">
                <Form.Check
                  type="checkbox"
                  label="Post Anonymously"
                  checked={isAnon}
                  onChange={(e) => setIsAnon(e.target.checked)}
                />
              </Form.Group>
              <Button type="submit" variant="primary">
                Post Review
              </Button>
            </Form>
          </Card>
        );*/
      }
    })

  }, [productId, user.id]);

  const renderStars = () => {
    const fullStars = Math.floor(product.averageRating);
    const halfStars = Math.ceil(product.averageRating - fullStars);
    const emptyStars = 5 - fullStars - halfStars;

    const stars = [];

    for (let i = 0; i < fullStars; i++) {
      stars.push(<FontAwesomeIcon key={i} icon={faStarSolid} style={{ color: 'gold' }} />);
    }

    for (let i = 0; i < halfStars; i++) {
      stars.push(<FontAwesomeIcon key={i + fullStars} icon={faStarRegular} style={{ color: 'gold' }} />);
    }

    for (let i = 0; i < emptyStars; i++) {
      stars.push(<FontAwesomeIcon key={i + fullStars + halfStars} icon={faStarRegular} style={{ color: '#d3d3d3' }} />);
    }

    return stars;
  };

  return (
      <>
      <Card className={product.isActive ? '' : 'bg-dark text-light'}>
        <Card.Img 
        variant="top" 
        src={product.image || "https://via.placeholder.com/200"}
        alt={product.name} />
        <Card.Body>
          <Card.Title>{product.name}</Card.Title>
          <Card.Text>{product.description}</Card.Text>
          <Card.Text>
            {product.salePercent ? (
              <>
                <span style={{ textDecoration: 'line-through' }}>
                  ${product.price}
                </span>{' '}
                <span>${product.finalPrice}</span>{' '}
                <span>({product.salePercent}% off)</span>
              </>
            ) : (
              <span>${product.price}</span>
            )}
          </Card.Text>
          {!user.isAdmin && (
            <>
              <ButtonGroup className="m-2">
                <Button
                  variant="secondary"
                  onClick={() => handleQuantityChange(quantity - 1)}
                >
                  -
                </Button>
                <Button variant="light">{quantity}</Button>
                <Button
                  variant="secondary"
                  onClick={() => handleQuantityChange(quantity + 1)}
                >
                  +
                </Button>
              </ButtonGroup>
              <Button
                variant="primary"
                className="m-2"
                onClick={handleAddToCart}
              >
                Add to Cart
              </Button>
              <Button variant="danger" onClick={handleBuyNow} className="m-2">
                Buy Now
              </Button>
            </>
          )}
          {user.isAdmin && product.isActive && (
            <>
              <Button
                variant="primary"
                className="m-2"
                href={`/products/${product._id}/update`}
              >
                Update Product
              </Button>
              <Button variant="danger" onClick={handleDeactivate}>
                Deactivate Product
              </Button>
            </>
          )}
          {user.isAdmin && !product.isActive && (
            <Button variant="warning" onClick={handleRestore} className="m-2">
              Restore Product
            </Button>
          )}
          <div className="my-3">{renderStars()}</div>
          <Card.Text>
            Stock: <strong>{product.stock}</strong>
          </Card.Text>
        </Card.Body>
      </Card>

      {isBought &&(
      <Card className="mt-4">
        <Form onSubmit={handleReview} className="m-3">
          <Form.Group controlId={"starRating"} className="mt-2 mb-2">
            <Form.Label>Rating</Form.Label>
            <div>
              {Array.from({ length: 5 }, (_, i) => (
                <FontAwesomeIcon
                  key={i}
                  icon={i < rating ? faStarSolid : faStarRegular}
                  onClick={() => {
                      setRating(i + 1);
                    }}

                  style={{ 
                    cursor: "pointer",
                    color: i < rating ? "gold" : "gray" 
                  }}
                />
              ))}
            </div>
          </Form.Group>
          <Form.Group controlId="review" className="mt-2 mb-2">
            <Form.Label>Review</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              value={review}
              onChange={(e) => setReview(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="anonymous" className="mt-2 mb-2">
            <Form.Check
              type="checkbox"
              label="Post Anonymously"
              checked={isAnon}
              onChange={(e) => setIsAnon(e.target.checked)}
            />
          </Form.Group>
          <Button type="submit" variant="primary" className="mt-2 mb-2">
            Post Review
          </Button>
        </Form>
      </Card>
      )}
      <FeedbackCard />
      </>
    );

}