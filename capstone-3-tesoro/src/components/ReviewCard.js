// importing from npm packages
import {useContext,useState} from "react";
import { Card, Button, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { faStar as faStarSolid } from "@fortawesome/free-solid-svg-icons";
import { faStar as faStarRegular } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// importing from within the app
import UserContext from "../UserContext"

export default function ReviewCard()
{
  const {user} = useContext(UserContext);

  //useStates for reviews
  const [isAnon, setIsAnon] = useState(false);
  const [rating, setRating] = useState(0);
  const [review, setReview] = useState("");
  const [reviewId, setReviewId] = useState("");

  // handling review
  function handleReview(e) 
  {
    e.preventDefault();

    console.log(rating);
    console.log(review);
    console.log(reviewId)
    console.log(isAnon);

    fetch(`${process.env.REACT_APP_API_URL}/products/${reviewId}/review`,
    {
          method: "PUT",
          headers:
          {
            "Content-Type" : "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`
          },
          body: JSON.stringify(
          {
            orderId: _id,
            userRating: rating,
            userReview: review,
            isAnon: isAnon
          })
        }).then(res =>{console.log(res); res.json()}).then(data =>
        {
          console.log(data)
          if (data.error)
          {
            Swal.fire({
              title: `${data.error}`,
                      icon: "error",
                      // text: "Please provide a different email."
            })
          }
          else
          {
            Swal.fire({
                      title: `${data.success}`,
                      icon: "success",
                      // text: "Please proceed to login."
                    })
            
          }
        })
  }
  
  return(
    <div className="p-2">
      <Form onSubmit={handleReview} >
        <Form.Group controlId={"starRating"}>
          <Form.Label>Star Rating</Form.Label>
          <div>
            {Array.from({ length: 5 }, (_, i) => (
              <FontAwesomeIcon
                key={i}
                icon={i < rating ? faStarSolid : faStarRegular}
                onClick={() => {
                    setRating(i + 1);
                    setReviewId(product.productId);
                  }}

                style={{ 
                  cursor: "pointer",
                  color: i < rating ? "gold" : "gray" 
                }}
              />
            ))}
          </div>
        </Form.Group>
        <Form.Group controlId="review">
          <Form.Label>Review</Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            value={review}
            onChange={(e) => setReview(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="anonymous">
          <Form.Check
            type="checkbox"
            label="Post Anonymously"
            checked={isAnon}
            onChange={(e) => setIsAnon(e.target.checked)}
          />
        </Form.Group>
        <Button type="submit" variant="primary">
          Post Review
        </Button>
      </Form>
    </div>
  );
}

