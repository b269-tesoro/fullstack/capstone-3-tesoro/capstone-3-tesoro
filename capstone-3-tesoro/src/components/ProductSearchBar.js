// importing from packages
import { useState, useEffect } from 'react';
import { useNavigate, NavLink } from 'react-router-dom';
import { Navbar, Nav, Form, FormControl, Button, Offcanvas, Container, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilter } from '@fortawesome/free-solid-svg-icons';

// importing from within the app
import ShawpLogo from "../images/ShawpLogo.png";
// import CategoryOptions from "../components/CategoryOptions"

function ProductSearchBar() {
  const [showFilter, setShowFilter] = useState(false);

  const navigate = useNavigate();

  const toggleFilter = () => setShowFilter((prevState) => !prevState);

  // for filter input fields
  const [keyword, setKeyword] = useState("");
  const [categories, setCategories] = useState("");
  const [onSale, setOnSale] = useState("");
  const [minPrice, setMinPrice] = useState("");
  const [maxPrice,setMaxPrice] = useState("");
  const [sortBy, setSortBy] = useState("")

  const [catOptions, setCatOptions] = useState("");


  const handleSubmit = (event) => 
  {
    event.preventDefault();

     navigate("/products/search", { state:  
      {
        keyword: keyword,
        category: categories,
        onSale: onSale,
        minPrice: minPrice,
        maxPrice: maxPrice,
        sortBy: sortBy
    }  });
      
  };

  useEffect(()=>
    {
      fetch(`${process.env.REACT_APP_API_URL}/products/categories`)
      .then(res => res.json())
      .then(data =>
      {
        setCatOptions(data.map(category =>
        {
          return (<option key={category} value={category}>{category}</option>)
        }))
      })

      // console.log({catOptions})
    },[])

  return (
    <Navbar bg="light" expand="lg" fixed="top" className="justify-content-center">
    	<Navbar.Brand as={NavLink} to="/" className="mr-3 ms-3">
    	  <img src={ShawpLogo} height="30" alt="Shawp Logo" className="d-inline-block align-top mr-2" />
    	  <span className="ms-2">Shawp</span>
    	</Navbar.Brand>
      <Container>
        <Row className="container-fluid">
          <Col xs={8} sm={6} lg={4}>
            <Form onSubmit={handleSubmit}  className="d-flex align-items-center">
              <FormControl 
                type="text" 
                placeholder="Search" 
                className="mr-sm-2 m-2 flex-grow-1"
                value={keyword}
                onChange={e=> setKeyword(e.target.value)} 
              />
              <Button type="submit" variant="primary">Search</Button>
            </Form>
          </Col>
          <Col xs={2} sm={2} lg={2} className="ms-auto">
            <Nav>
              <Button variant="light" onClick={toggleFilter} className="ms-auto">
                <FontAwesomeIcon icon={faFilter} />
              </Button>
            </Nav>
          </Col>
        </Row>
      </Container>

      <Offcanvas show={showFilter} onHide={toggleFilter} placement="end">
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Filter Options</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <Form onSubmit={handleSubmit}className="p-2">
            <Form.Group controlId="keyword" className="p-2">
              <Form.Label>Keyword</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Enter keyword"
                value={keyword}
                onChange={e=> setKeyword(e.target.value)} 
              />
            </Form.Group>

            <Form.Group controlId="category" className="p-2">
              <Form.Label>Category</Form.Label>
              <Form.Control as="select" onChange={e=> setCategories(e.target.value)}>
                <option value="">All categories</option>
                {catOptions}
              </Form.Control>

            </Form.Group>

            <Form.Group controlId="onSale" className="p-2">
              <Form.Check type="checkbox" label="On sale only" value={onSale} onChange={e=> setOnSale(e.target.checked)} />
            </Form.Group>

            <Form.Group controlId="minPrice" className="p-2">
              <Form.Label>Minimum price</Form.Label>
              <Form.Control 
                type="number" 
                placeholder="1"
                value={minPrice}
                onChange={e=> setMinPrice(e.target.value)}  
              />
            </Form.Group>

            <Form.Group controlId="maxPrice" className="p-2">
              <Form.Label>Maximum price</Form.Label>
              <Form.Control 
                type="number" 
                placeholder="200"
                value={maxPrice}
                onChange={e=> setMaxPrice(e.target.value)} 
              />
            </Form.Group>

            <Form.Group controlId="sortBy" className="p-2">
              <Form.Label>Sort by</Form.Label>
              <Form.Control as="select" onChange={e=> setSortBy(e.target.value)}>
                <option value="">Default sorting</option>
                <option value="price">Price</option>
                <option value="reviews">Reviews</option>
                <option value="buyCount">Popular</option>
              </Form.Control>
            </Form.Group>
            <div className="text-center">
            <Button type="submit" variant="primary" className="mr-2 p-2">Search</Button>
            </div>
          </Form>
        </Offcanvas.Body>
      </Offcanvas>
    </Navbar>
  );
}

export default ProductSearchBar;
