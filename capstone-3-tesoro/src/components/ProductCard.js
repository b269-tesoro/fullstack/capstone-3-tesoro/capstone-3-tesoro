// importing from npm packages
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar as faStarSolid } from "@fortawesome/free-solid-svg-icons";
import { faStar as faStarRegular } from "@fortawesome/free-solid-svg-icons";



const ProductCard = ({ product }) => 
{
  const 
  {
    _id,
    image,
    name,
    description,
    price,
    salePercent,
    finalPrice,
    averageRating,
    // stock,
    buyCount,
    isActive
  } = product;

  const renderStars = () => 
  {
    const fullStars = Math.floor(averageRating);
    const halfStars = Math.ceil(averageRating - fullStars);
    const emptyStars = 5 - fullStars - halfStars;

    const stars = [];

    for (let i = 0; i < fullStars; i++) {
      stars.push(
        <FontAwesomeIcon key={i} icon={faStarSolid} style={{ color: "gold" }} />
      );
    }

    for (let i = 0; i < halfStars; i++) {
      stars.push(
        <FontAwesomeIcon
          key={i + fullStars}
          icon={faStarRegular}
          style={{ color: "gold" }}
        />
      );
    }

    for (let i = 0; i < emptyStars; i++) {
      stars.push(
        <FontAwesomeIcon
          key={i + fullStars + halfStars}
          icon={faStarRegular}
          style={{ color: "gray" }}
        />
      );
    }

    return stars;
  };

  return (
    <Card style={{ width: "18rem" }} className={isActive ? 'm-2' : 'bg-dark text-light m-2'}>
      <Link to={`/products/${_id}`} className="d-flex justify-content-center">
        <Card.Img
          variant="top"
          src={image || "https://via.placeholder.com/200"}
          alt={name}
          className="mt-3"
          style={{ width: "16rem", height: "16rem" }}
        />
      </Link>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text 
        style={{whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis"}}
        >{description}</Card.Text>
        {salePercent ? (
          <div>
            <span
              style={{
                textDecoration: "line-through",
                marginRight: "10px",
              }}
            >
              ${price.toFixed(2)}
            </span>
            ${finalPrice.toFixed(2)} ({salePercent}% off)
          </div>
        ) : (
          <div>${price.toFixed(2)}</div>
        )}
        {buyCount > 0 && <Card.Text>Products Sold: {buyCount}</Card.Text>}
        <Card.Text>{renderStars()}</Card.Text>
      </Card.Body>
    </Card>
  );


};

export default ProductCard;
