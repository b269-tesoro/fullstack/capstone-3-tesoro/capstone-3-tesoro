// importing from npm packages
import {useContext,useState} from "react";
import { Card, Button, Form } from 'react-bootstrap';
import { Link/*, useNavigate*/ } from "react-router-dom";
import Swal from 'sweetalert2';
// import { faStar as faStarSolid } from "@fortawesome/free-solid-svg-icons";
// import { faStar as faStarRegular } from "@fortawesome/free-solid-svg-icons";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// importing from within the app
import UserContext from "../UserContext"

const OrderCard = ({ order }) => {
  const { _id, /*userId,*/ products, totalAmount, purchasedOn, orderStatus } = order;

  const {user} = useContext(UserContext);
  // const navigate = useNavigate();
  
  const [status, setStatus] = useState(orderStatus);

  let statusMessage;
  switch (orderStatus) {
    case 0:
      statusMessage = 'To Ship';
      break;
    case 1:
      statusMessage = 'Packed';
      break;
    case 2:
      statusMessage = 'Shipped';
      break;
    case 3:
      statusMessage = 'Out for Delivery';
      break;
    case 4:
      statusMessage = 'Delivered';
      break;
    case 5:
      statusMessage = 'Confirmed Receipt';
      break;
    default:
      statusMessage = 'Unknown';
  }

  // confirming receipt from user side
  function handleConfirmReceipt(e)
  {
  	  	fetch(`${process.env.REACT_APP_API_URL}/orders/${_id}/confirm`,
  	  	{
  	  		method: "PUT",
  	  		headers:
  	  		{
  	        Authorization: `Bearer ${localStorage.getItem("token")}`
  	  		}
  	  	}).then(res =>res.json()).then(data =>
  	    {
  	      // console.log(data)
  	      if (data.error)
  	      {
  	        Swal.fire({
  	          title: `${data.error}`,
  	                  icon: "error",
  	                  // text: "Please provide a different email."
  	        })
  	      }
  	      else
  	      {
  	        Swal.fire({
  	                  title: `${data.success}`,
  	                  icon: "success",
                      timer: 1000,
                      showConfirmButton: false
  	                  // text: "Please proceed to login."
  	                }).then(result=>
                    {
                      window.location.reload()
                     // navigate("/")
                    })
  	      }
  	    })
  }
  // updating shipping status from admin side
  function handleStatusSubmit(e)
  {
  	e.preventDefault();
  	fetch(`${process.env.REACT_APP_API_URL}/orders/${_id}/status`,
  	{
  		method: "PUT",
  		headers:
  		{
				"Content-Type" : "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
  		},
  		body: JSON.stringify(
      {
        orderStatus: status
      })
  	}).then(res =>res.json()).then(data =>
    {
      // console.log(data)
      if (data.error)
      {
        Swal.fire({
          title: `${data.error}`,
                  icon: "error",
                  // text: "Please provide a different email."
        })
      }
      else
      {
        Swal.fire({
                  title: `${data.success}`,
                  icon: "success",
                  // text: "Please proceed to login."
                })
      }
    })
  }
  return (
    <Card className="m-2">
      <Card.Header>Order Details</Card.Header>
      <Card.Body>
        <Card.Title>Total Amount: ${totalAmount.toFixed(2)}</Card.Title>
        <Card.Text>Purchased On: {purchasedOn}</Card.Text>
        <Card.Text>Status: {statusMessage}</Card.Text>
        <ul>
          {products.map((product) => (
            <li key={product.productId}>
              <p>{product.name}</p>
              <p>Description: {product.description}</p>
              {product.salePercent ? (
                <div>
                  <span
                    style={{
                      textDecoration: "line-through",
                      marginRight: "10px",
                    }}
                  >
                    ${product.price.toFixed(2)}
                  </span>
                  ${product.finalPrice.toFixed(2)} ({product.salePercent}% off)
                </div>
              ) : (
                <div>${product.price.toFixed(2)}</div>
              )}
              <p>Quantity: {product.quantity}</p>
              <p>Subtotal: {product.subTotal.toFixed(2)}</p>
              {user.isAdmin ? (
                ""
              ) : status === 5 ? (
                <div className="text-center  m-3">
                    <Button as={Link}to={`/products/${product.productId}`} variant="primary">
                      Review
                    </Button>
                </div>
              ) : (
                ""
              )}
            </li>
          ))}
        </ul>
        {user.isAdmin && (
          <Form onSubmit={handleStatusSubmit}>
            <Form.Group controlId="orderStatus" className="p-2">
              <Form.Label>Order Status</Form.Label>
              <Form.Control as="select" value={status} onChange={e=> setStatus(e.target.value)}>
                <option value={0}>To Ship</option>
                <option value={1}>Packed</option>
                <option value={2}>Shipped</option>
                <option value={3}>Out for Delivery</option>
                <option value={4}>Delivered</option>
              </Form.Control>
            </Form.Group>
            <Button type="submit" variant="primary" className="text-center  m-3">
              Update Order Status
            </Button>
          </Form>
        )}
        {!user.isAdmin && statusMessage === "Delivered" && (
          <Button variant="primary" onClick={handleConfirmReceipt}>
            Confirm Receipt
          </Button>
        )}
      </Card.Body>
    </Card>
  );


};

export default OrderCard;
