import {Outlet} from "react-router-dom";

export default function NoNav() {return <Outlet/>}