import { useContext } from 'react';
import { Card, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';
import ProfilePic from "../images/ProfilePic.png";
import ProfilePic2 from "../images/ProfilePic2.png";


export default function UserCard({userInfo})
{
  const{user} = useContext(UserContext);
  const
  {
    _id ,
    email ,
    username ,
    isAdmin 
  } = userInfo;

  function elevateToAdmin(e)
  {
    if(user.isAdmin)
    {
      Swal.fire({
        title: 'Are you sure?',
        text: 'Elevating a user to admin cannot be undone.',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes!',
        cancelButtonText: 'Cancel',
      }).then((result) => {
        if (result.isConfirmed) 
        {
          fetch(`${process.env.REACT_APP_API_URL}/users/${_id}/elevate`,
          {
            method:"POST",
            headers:
                  {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                  }
          }).then(res=>res.json()).then(data=> 
          {
            console.log(data)
            if (data.error)
            {
              Swal.fire({
                title: `${data.error}`,
                icon: "error",
                // text: "Please provide a different email."
              })
            }
            else
            {
              Swal.fire({
              title: `${data.success}`,
              icon: "success",
              // text: "Please proceed to login."
              })

              window.location.reload();
            }
          })
        }
      });
    }
  }

  return(
    <Card className=
    {isAdmin ? "bg-warning p-5 justify-content-center dashboardCard flex-column mb-5" : "p-5 justify-content-center dashboardCard flex-column mb-5"} 
    id={_id}>
      <div className="d-flex justify-content-center align-items-center flex-wrap">
        <div className="p-0">
          <Card.Img variant="top" src={isAdmin ?ProfilePic : ProfilePic2 } className="card-img p-3" style={{ width: '200px', height: '200px' }} />
        </div>
        <div className="p-4">
          <Card.Title className="card-title">{username}</Card.Title>
          <Card.Title className="card-title">{email}</Card.Title>
          {isAdmin ? <Card.Title className="card-title text-danger">Admin</Card.Title> : <Button onClick={elevateToAdmin}variant="warning">Elevate to Admin</Button>}
          
        </div>
      </div>
    </Card>
  )
}
		