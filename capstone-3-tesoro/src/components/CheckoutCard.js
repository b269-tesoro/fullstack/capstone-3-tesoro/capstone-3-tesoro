import {useContext, /*useState, useEffect*/} from "react";
import {Card, Button} from 'react-bootstrap';
import { useNavigate } from "react-router-dom";
import Swal from 'sweetalert2';

// importing from within the app
import UserContext from "../UserContext"


export default function CheckoutCard({total})
{
  const {user} = useContext(UserContext);
  const navigate = useNavigate();
  // const [page, setPage] = useState();

  function checkOutCart(e)
  {
    fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/cart/checkout`,
    {
      method: "POST",
      headers:
      {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
  .then(res => res.json()).then(data =>
    {
      console.log(data)
      if (data.error)
      {
        Swal.fire({
          title: `${data.error}`,
                  icon: "error",
                  // text: "Please provide a different email."
        })
      }
      else
      {
        Swal.fire({
                  title: `${data.success}`,
                  icon: "success",
                  timer: 2000,
                  showConfirmButton: false
                  // text: "Please proceed to login."
                })
        
         navigate("/orders")
      }
    })
  }

  return(
	<Card className="border-0 d-flex m-2 justify-content-center mb-5" style={{ width: "18rem" }}>
  	<Card.Body className="d-flex m-2 justify-content-center">
      <Card.Text className="d-flex m-2 justify-content-center">
        Total: ${total.toFixed(2)}
      </Card.Text>
        <Button variant="primary" onClick={checkOutCart}>Check Out</Button>
    </Card.Body>
  </Card>
  )
}