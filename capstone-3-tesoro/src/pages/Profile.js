import { useContext,useState, useEffect } from 'react';
import { Card, Button, Modal, Form, Container } from 'react-bootstrap';
// import {useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2';

import UserContext from '../UserContext';
import ProfilePic from "../images/ProfilePic.png";
import ProfilePic2 from "../images/ProfilePic2.png";

export default function Profile()
{
	const {user} = useContext(UserContext);
  // const navigate = useNavigate();

	const [showModal, setShowModal] = useState(false);
	const [password, setPassword] = useState("");
	const [newPassword, setNewPassword] = useState("");
	const [verifyNewPassword, setVerifyNewPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

	function updatePassword(e)
	{
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/updatepassword`,
		{
			method: "POST",
			headers:
			{
				"Content-Type" : "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify(
      {
        password: password,
        newPassword: newPassword
      })
		}).then(res=> res.json()).then(data=>
		{
			 console.log(data)
			 console.log(password)
			 console.log(newPassword)
      if (data.error)
      {
        Swal.fire({
          title: `${data.error}`,
                  icon: "error",
                  // text: "Please provide a different email."
        })
      }
      else
      {
        Swal.fire({
                  title: `${data.success}`,
                  icon: "success",
                  // text: "Please proceed to login."
                })
        setPassword("");
        setNewPassword("");
        setVerifyNewPassword("");
        setShowModal(false);  
      }
		}) 
	}

  useEffect(()=>
  {
    if (
      password !== "" &&
      newPassword !== "" &&
      verifyNewPassword !== "" &&
      newPassword === verifyNewPassword)
    {
      setIsActive(true);
    }
    else
    {
      setIsActive(false)
    }

  },[password,newPassword,verifyNewPassword])

	return(
		<>
		<Container className="overflow-auto">
		<Card className="p-5 justify-content-center dashboardCard flex-column mb-5">
      <div className="d-flex justify-content-center align-items-center flex-wrap">
        <div className="p-0">
          <Card.Img variant="top" src={user.isAdmin ?ProfilePic : ProfilePic2 } className="card-img p-3" style={{ width: '200px', height: '200px' }} />
        </div>
        <div className="p-4">
          <Card.Title className="card-title">{user.username}</Card.Title>
          <Card.Title className="card-title">{user.email}</Card.Title>
          <Button onClick={() => setShowModal(true)}>Change Password</Button>
        </div>
      </div>
    </Card>

      <Modal show={showModal} onHide={() => setShowModal(false)} className="d-flex flex-wrap">
        <Modal.Header closeButton>
          <Modal.Title>Change Password</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={updatePassword}>
            <Form.Group controlId="password" className="p-2">
              <Form.Label>Current Password</Form.Label>
              <Form.Control type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="newPassword" className="p-2">
              <Form.Label>New Password</Form.Label>
              <Form.Control type="password" value={newPassword} onChange={(e) => setNewPassword(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="verifyNewPassword" className="p-2">
              <Form.Label>Verify New Password</Form.Label>
              <Form.Control type="password" value={verifyNewPassword} onChange={(e) => setVerifyNewPassword(e.target.value)} />
            </Form.Group>
            {isActive?
            <div className="text-center m-3">
            <Button variant="primary" type="submit">Confirm</Button>
            </div>
            :
            <div className="text-center m-3">
            <Button variant="secondary" type="submit" disabled>Confirm</Button>
            </div>
            }
            
          </Form>
        </Modal.Body>
      </Modal>
    
    </Container>
    </>
		)
}
