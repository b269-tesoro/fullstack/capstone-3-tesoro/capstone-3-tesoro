// importing from packages
import {useState, useEffect} from "react";
import {useLocation} from 'react-router-dom';
import {Row, Card, Container} from "react-bootstrap";

// importing from within the app
import ProductCard from "../components/ProductCard"
// import ProductSearchBar from "../components/ProductSearchBar"


export default function ProductsSearch()
{
	// for add to cart function
	const { state } = useLocation();

	// useState for placing products into cards for rendering
	const [productsResult, setProductsResult] = useState();
	const [isEmpty, setIsEmpty] = useState(true);

	// console.log({state})

	useEffect(()=>
	{
		
		fetch(`${process.env.REACT_APP_API_URL}/products/search`,
		      {
		        method: "POST",
		        headers:
		        {
		          "Content-Type" : "application/json"
		        },
		        body: JSON.stringify(
		        {
		          keyword: state.keyword,
		          category: state.category,
		          onSale: state.onSale,
		          minPrice: state.minPrice,
		          maxPrice: state.maxPrice,
		          sortBy: state.sortBy
		        })
		      })
		      .then(res => res.json())
		      .then(data =>
		      {
	         console.log(data)
	         if(data)
	         {
	         	setIsEmpty(false);
	         	setProductsResult(data.map(product => 
		        {
		          return (<ProductCard key={product._id} product={product}/>)
		        }));
	         }
	         else
	         {
	         	setIsEmpty(true);
	         	setProductsResult([]);
	         }
		        
		      })	
	},[state])
	console.log({isEmpty})
	return (
		<>
		{isEmpty ? (
    <Card className="border-0 d-flex m-2 text-center">
  	  <Card.Body className="d-flex m-2 justify-content-center">
  	    <Card.Text className="d-flex m-2 justify-content-center">
  	      No items match your search.
  	    </Card.Text>
  	  </Card.Body>
  	</Card>
  	):(
  	
  	<Container className="justify-content-center">
      <Row xs={1} sm={2} md={3} lg={4} className="justify-content-center">
            {productsResult}
      </Row>
    </Container>
  	)}
  	</>
  );
}