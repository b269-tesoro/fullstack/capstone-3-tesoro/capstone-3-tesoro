// importing from packages
import {useState, useEffect, useContext} from "react";
import {Row, /*Col,*/ Container} from "react-bootstrap";
import {useNavigate} from 'react-router-dom';

// importing from within the app
import UserContext from "../UserContext"
import ProductCard from "../components/ProductCard"

export default function Archives()
{
	// for add to cart function
	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	
	// useState for placing products into cards for rendering
	const [products, setProducts] = useState();
		
	useEffect(()=>
	{
		if(user.isAdmin)
		{
			fetch(`${process.env.REACT_APP_API_URL}/products/archives/all`,
				{
					headers:
					{
					  Authorization: `Bearer ${localStorage.getItem("token")}`
					}
				})
			.then(res => res.json())
			.then(data =>
			{
				// console.log(data)
				setProducts(data.map(product =>
				{
					return (<ProductCard key={product._id} product={product}/>)
				}))
			})
		}
		else
		{
			navigate("/");
		}
		
	},[user.isAdmin, navigate])	
	// console.log({products})
	/*return (
		<>
		{products}
		</>
	)*/


	return (
	    <Container className="justify-content-center">
	      <Row xs={1} sm={2} md={3} lg={4} className="justify-content-center">
	            {products}
	      </Row>
	    </Container>
	  );

}