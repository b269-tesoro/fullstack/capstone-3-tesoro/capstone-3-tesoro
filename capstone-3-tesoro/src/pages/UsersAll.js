// importing from npm packages
import { useContext, useState, useEffect } from 'react';
import {useNavigate} from 'react-router-dom';
import { Container } from 'react-bootstrap';

// importing from within the app
import UserContext from '../UserContext';
import UserCard from '../components/UserCard';

export default function UsersAll()
{
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const [cards, setCards] = useState();

	useEffect(()=>
	{
		if(user.isAdmin)
		{
			fetch(`${process.env.REACT_APP_API_URL}/users/all`,
				{
					headers:
					{
					  Authorization: `Bearer ${localStorage.getItem("token")}`
					}
				})
			.then(res => res.json())
			.then(data =>
			{
				// console.log(data)
				setCards(data.map(userInfo =>
				{
					return (<UserCard key={userInfo._id} userInfo={userInfo}/>)
				}))
			})
		}
		else
		{
			navigate("/");
		}
	},[user.isAdmin, navigate])

	return(
		<>
		<Container className="overflow-auto">
		{cards}
		</Container>
		</>
	)

}