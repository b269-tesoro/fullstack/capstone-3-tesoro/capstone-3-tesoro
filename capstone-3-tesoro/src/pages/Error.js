import { Card } from 'react-bootstrap';
import Page404 from "../images/Page404.png";

export default function Error()
{
	return(
		<Card className="p-5 justify-content-center dashboardCard flex-column mb-5 border-0 bg-transparent">
		  <div className="d-flex justify-content-center align-items-center">
		    <div className="p-0">
		      <Card.Img variant="top" src={Page404} className="card-img p-3" style={{ width: '200px', height: '200px' }} />
		    </div>
		    <div className="p-4">
		      <h1>404 Page Not Found.</h1>
		    </div>
		  </div>
		</Card>
	);
}