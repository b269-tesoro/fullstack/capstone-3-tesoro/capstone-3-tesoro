// importing from packages
import {useState, useEffect, useContext} from "react";
import {Row, /*Col,*/ Container, Card, /*Button*/} from "react-bootstrap";
// import Swal from 'sweetalert2';
// import {useNavigate} from 'react-router-dom';

// importing from within the app
import UserContext from "../UserContext"
import CartCard from "../components/CartCard"
import CheckoutCard from "../components/CheckoutCard"

export default function Cart()
{
	// for add to cart function
	const {user} = useContext(UserContext);
	// const navigate = useNavigate();
	// const { state } = useLocation();

	// useState for placing products into cards for rendering
	const [products, setProducts] = useState();
	const [total, setTotal] = useState(0);
	const [page, setPage] = useState();

	// moved to CheckoutCard
	/*function checkOutCart(e)
	{
		fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/cart/checkout`,
		{
			method: "POST",
			headers:
			{
			  Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
	.then(res => res.json()).then(data =>
    {
      console.log(data)
      if (data.error)
      {
        Swal.fire({
          title: `${data.error}`,
                  icon: "error",
                  // text: "Please provide a different email."
        })
      }
      else
      {
        Swal.fire({
                  title: `${data.success}`,
                  icon: "success",
                  timer: 2000,
                  showConfirmButton: false
                  // text: "Please proceed to login."
                })
        
         navigate("/orders")
      }
    })
	}*/

	useEffect(()=>
	{
		console.log(user.id)
		fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/cart`,
		{
			method: "POST",
			headers:
			{
			  Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data =>
		{
			console.log(`total: ${data.TotalAmount}`)
			if(data.TotalAmount>0)
			{
				// console.log(total);
				setTotal(data.TotalAmount);
				
				setProducts(data.cart.map(product =>
				{
					return (<CartCard key={product.productId._id} product={product}/>)
				}))

				setPage(
					<CheckoutCard total={data.TotalAmount}/>
				)
				
			}

			else
			{
				setTotal(data.TotalAmount);

				return(
					setProducts(
					<Card className="border-0 d-flex m-2 justify-content-center">
					  <Card.Body className="d-flex m-2 justify-content-center">
					    <Card.Text className="d-flex m-2 justify-content-center">
					      Nothing in cart.
					    </Card.Text>
					  </Card.Body>
					</Card>
					)
					
				);

				// setPage(null)
			}

			
			
		})			
	},[user.id])	
	console.log({products,total})
	/*return (
		<>
		{products}
		</>
	)*/


	return (
	    <Container className="justify-content-center mt-4">
	      <Row xs={1} sm={2} md={3} lg={4} className="justify-content-center">
	        {products}
	      </Row>

	      <Row xs={12} className="justify-content-center">
	      	{page}
	      </Row>
	    </Container>

	  );

}