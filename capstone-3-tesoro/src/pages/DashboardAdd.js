import { useState, useContext, /*useEffect*/ } from "react";
import { Form, Button, Card } from "react-bootstrap";
import {/*Navigate,*/ useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';


import UserContext from "../UserContext"
export default function DashboardAdd() 
{
  const [image, setImage] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [category, setCategory] = useState("");
  const [salePercent, setSalePercent] = useState("");
  const [stock, setStock] = useState("");
  // const [product, setProduct] = useState({});

  const {user} = useContext(UserContext);
	const navigate = useNavigate();

  // const handleSubmit = (event) => 
  // {
  //   event.preventDefault();

  //   setProduct ({
  //     name,
  //     description,
  //     price,
  //     category: category.split(","),
  //     salePercent,
  //     stock
  //   });

  //   console.log(product);
  //   CreateNewProduct(product);
  // }
    

    function CreateNewProduct(e)
    {
    	    e.preventDefault();
      if(user.isAdmin)
      {
    	fetch(`${process.env.REACT_APP_API_URL}/products/new`,
    	{
    		method: "POST",
    		headers:
    		      {
    		      	"Content-Type" : "application/json",
    		        Authorization: `Bearer ${localStorage.getItem("token")}`
    		      },
    		body: JSON.stringify(
    					{
    						image: image,
    						name: name,
					      description: description,
					      price: price,
					      category: category.split(/\s*,\s*/),
					      salePercent: salePercent,
					      stock: stock
    					}) 
    	}).then(res =>res.json()).then(data =>
			{
				console.log(data)
				if (data.error)
				{
					Swal.fire({
						title: `${data.error}`,
                    icon: "error",
                    // text: "Please provide a different email."
					})
				}
				else
				{
					Swal.fire({
                    title: `${data.success}`,
                    icon: "success",
                    // text: "Please proceed to login."
                	})

					console.log(data.product)
        	navigate(`/products/${data.product._id}`);
				}
			})
      }
  	};

  return (
    <div className="d-flex justify-content-center mt-5">
      <Card style={{ width: "25rem" }}>
        <Card.Body>
          <Card.Title className="text-center p-2">Create Product</Card.Title>
          <Form onSubmit={(e) => CreateNewProduct(e)}>

          	<Form.Group controlId="formProductImage" className="p-2">
          	  <Form.Label>Product Image</Form.Label>
          	  <Form.Control
          	    type="text"
          	    placeholder="Enter product image url"
          	    value={image}
          	    onChange={(e) => setImage(e.target.value)}
          	    required
          	  />
          	</Form.Group>

            <Form.Group controlId="formProductName" className="p-2">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product name"
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="formProductDescription" className="p-2">
              <Form.Label>Product Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                placeholder="Enter product description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="formProductPrice" className="p-2">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="formProductCategory" className="p-2">
              <Form.Label>Category</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter categories separated by commas"
                value={category}
                onChange={(e) => setCategory(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="formProductSalePercent" className="p-2">
              <Form.Label>Sale Percent</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter sale percent"
                value={salePercent}
                onChange={(e) => setSalePercent(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="formProductStock" className="p-2">
              <Form.Label>Stock</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter stock"
                value={stock}
                onChange={(e) => setStock(e.target.value)}
                required
              />
            </Form.Group>

            <div className="text-center m-3">
            <Button variant="primary" type="submit">
              Create Product
            </Button>
            </div>

          </Form>
        </Card.Body>
      </Card>
    </div>
  );
}