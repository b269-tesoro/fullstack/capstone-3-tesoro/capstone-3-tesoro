// importing from packages
import {useState, useEffect, useContext} from "react";
import { Container, Row, Col, Button, Card } from "react-bootstrap";
import { NavLink } from "react-router-dom"
// importing from within the app
import UserContext from "../UserContext"
import ProductCard from "../components/ProductCard"
import ShawpLogo from "../images/ShawpLogo.png";
import Attribution from "../images/Attribution.png";


export default function Home ()
{
	const {user} = useContext(UserContext);
	const [hot, setHot] = useState();

	useEffect(()=>
	{
		fetch(`${process.env.REACT_APP_API_URL}/products/hot`)
		.then(res => res.json())
		.then(data =>
		{
			console.log(data)
			setHot(data.map(product =>
			{
				return (<ProductCard key={product._id} product={product}/>)
				// return (<ProductCard key={product._id} product={product}/>)
			}))
		})
	},[])
	console.log(user);
	return (
	<>
		<div className="mb-3">
      <Container fluid>
        <Row className="pt-5 pb-5">
          <Col className="text-center">
          	<div className="circle-border">
              <img src={ShawpLogo} alt="Shawp Logo" className="logo-img" style={{ width: '200px', height: '200px' }}/>
            </div>
            {user.username ? (
              <h1>Welcome to Shawp, {user.username}!</h1>
            ) : (
              <h1>Welcome to Shawp!</h1>
            )}
            <p>Your one-stop shop for top-notch props.</p>
            <Button variant="primary" as={NavLink} to="/products">
              Start Shawping
            </Button>
          </Col>
        </Row>
      </Container>
    </div>

    <div>
    <Container className="justify-content-center p-3">
    	<h1 className="text-center">Best Sellers</h1>
      <Row xs={1} sm={2} md={3} lg={4} className="justify-content-center">
            {hot}
      </Row>
    </Container>
    </div>

    <Container className="overflow-auto p-3">
      <Card className="p-5 justify-content-center dashboardCard flex-column mb-5" >
        <div className="d-flex justify-content-center align-items-center flex-wrap">
          <div className="p-0">
            <Card.Img variant="top" src={Attribution} className="card-img p-3" style={{ width: '200px', height: '200px' }} />
          </div>
          <div className="p-4">
            <h1 className="card-title" ><a href="https://www.flaticon.com/authors/freepik" className ="stretched-link" style={{color: 'inherit','text-decoration': 'none'}}>Like these icons?</a></h1>
            <Card.Text>They're free! Check out more from the creator Freepik.</Card.Text>
          </div>
        </div>
      </Card>
    </Container>
	</>
	)
}