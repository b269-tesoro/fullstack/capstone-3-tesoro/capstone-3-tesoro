// importing from npm packages
import { useContext,useState, useEffect } from 'react';
import { Card, Container } from 'react-bootstrap';
import {Link, useNavigate} from 'react-router-dom';
// importing from within the app
import UserContext from '../UserContext';
import AddProduct from "../images/AddProduct.png";
import AllProducts from "../images/AllProducts.png";
import Archives from "../images/Archives.png";
import AllOrders from "../images/AllOrders.png";
import AllUsers from "../images/AllUsers.png";

export default function Dashboard( )
{

	const {user} = useContext(UserContext);
  const navigate = useNavigate();
	const [page, setPage] = useState();
	useEffect(()=>
	{
    if(user.isAdmin)
    {
         setPage(
        <>
        <Container className="overflow-auto">
          <Card className="p-5 justify-content-center dashboardCard flex-column mb-5" as={Link} to="/dashboard/add">
            <div className="d-flex justify-content-center align-items-center flex-wrap">
              <div className="p-0">
                <Card.Img variant="top" src={AddProduct} className="card-img p-3" style={{ width: '200px', height: '200px' }} />
              </div>
              <div className="p-4">
                <Card.Title className="card-title">Add New Products</Card.Title>
              </div>
            </div>
          </Card>

          <Card className="p-5 justify-content-center dashboardCard flex-column mb-5" as={Link} to="/products">
            <div className="d-flex justify-content-center align-items-center flex-wrap">
              <div className="p-0">
                <Card.Img variant="top" src={AllProducts} className="card-img p-3" style={{ width: '200px', height: '200px' }} />
              </div>
              <div className="p-4">
                <Card.Title className="card-title">All Products</Card.Title>
              </div>
            </div>
          </Card>

          <Card className="p-5 justify-content-center dashboardCard flex-column mb-5" as={Link} to="/products/archives/all">
            <div className="d-flex justify-content-center align-items-center flex-wrap">
              <div className="p-0">
                <Card.Img variant="top" src={Archives} className="card-img p-3" style={{ width: '200px', height: '200px' }} />
              </div>
              <div className="p-4">
                <Card.Title className="card-title">Archives</Card.Title>
              </div>
            </div>
          </Card>

          <Card className="p-5 justify-content-center dashboardCard flex-column mb-5" as={Link} to="/orders/all">
            <div className="d-flex justify-content-center align-items-center flex-wrap">
              <div className="p-0">
                <Card.Img variant="top" src={AllOrders} className="card-img p-3" style={{ width: '200px', height: '200px' }} />
              </div>
              <div className="p-4">
                <Card.Title className="card-title">All Orders</Card.Title>
              </div>
            </div>
          </Card>

          <Card className="p-5 justify-content-center dashboardCard flex-column mb-5" as={Link} to="/users/all">
            <div className="d-flex justify-content-center align-items-center flex-wrap">
              <div className="p-0">
                <Card.Img variant="top" src={AllUsers} className="card-img p-3" style={{ width: '200px', height: '200px' }} />
              </div>
              <div className="p-4">
                <Card.Title className="card-title">All Users</Card.Title>
              </div>
            </div>
          </Card>
        </Container>
        </>

        );
    }
    else
    {
      navigate("/");
    }
	 
	},[user, navigate])
 
 	return(
 		<>
 		{page}
 		</>)
};
