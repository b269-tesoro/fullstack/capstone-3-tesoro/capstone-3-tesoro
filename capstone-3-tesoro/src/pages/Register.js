// importing from packages
import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import { Container, Row, Col, Card, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

// importing from within the app
import UserContext from '../UserContext';
import ShawpLogo from "../images/ShawpLogo.png";

export default function Register()
{
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const [email, setEmail] = useState("");
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [vPassword, setVPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	useEffect(() =>
	{
		if (email !== "" &&
			username !== "" &&
			password !== "" &&
			vPassword !== "" &&
			password === vPassword)
		{
			setIsActive(true);
		}
		else
		{
			setIsActive(false)
		}
		if(user.id)
		{
			navigate("/");
		}
	},[email, username, password, vPassword,user.id, navigate])

	function registerUser(e)
	{
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/register`,
		{
			method: "POST",
			headers:
			{
				"Content-Type" : "application/json"
			},
			body: JSON.stringify(
			{
				username: username,
				email: email,
				password: password
			})
		}).then(res =>res.json()).then(data =>
			{
				if (data.error)
				{
					Swal.fire({
						title: `${data.error}`,
                    icon: "error",
                    // text: "Please provide a different email."
					})
				}
				if (data.success)
				{
					Swal.fire({
                    title: `${data.success}`,
                    icon: "success",
                    text: "Please proceed to login."
                	})

                	navigate("/login");
				}
			})

		setUsername("");
		setEmail("");
		setPassword("");
		setVPassword("");
	}

	return(
		(user.id !== null)?
		<Navigate to ="/" /> 
		:
		<Container className="justify-content-center mt-3">
			<Row className="justify-content-center">
				<Col xs={12} lg={4}>
				<Card className="p-3">
				<div className="text-center	mb-5">
				<Card.Img variant="top" src={ShawpLogo} alt="Shawp Logo" style={{ width: '100px', height: '100px' }}/>
				</div>
				<Form.Text className="text-muted p-2" style={{textAlign: 'justify', fontStyle: 'italic', fontSize: '0.6em'}}>
				    Please note that while this server uses encryption to protect your information, this website is for demonstration purposes only and is not intended for actual use. Therefore, I cannot guarantee the security of any information entered on this site and strongly advise against using real credentials.
				</Form.Text>
				<Form onSubmit={(e) => registerUser(e)}>
					<Form.Group controlId="username" className="p-2">
					        <Form.Label>username</Form.Label>
					        <Form.Control 
					            type="text" 
					            placeholder="username" 
					            value={username}
					            onChange={e => setUsername(e.target.value)}
					            required
					        />
					    </Form.Group>

				    <Form.Group controlId="userEmail" className="p-2">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				            type="email" 
				            placeholder="Enter email" 
				            value={email}
				            onChange={e => setEmail(e.target.value)}
				            required
				        />
				        
				    </Form.Group>

				    <Form.Group controlId="password" className="p-2">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				            type="password" 
				            placeholder="Password" 
				            value={password}
				            onChange={e => setPassword(e.target.value)}
				            required
				        />
				    </Form.Group>

				    <Form.Group controlId="password2" className="p-2">
				        <Form.Label>Verify Password</Form.Label>
				        <Form.Control 
				            type="password" 
				            placeholder="Verify Password" 
				            value={vPassword}
				            onChange={e => setVPassword(e.target.value)}
				            required
				        />
				    </Form.Group>

				    { isActive ?
				    		<div className="text-center	m-3">
				        <Button variant="primary" type="submit" id="submitBtn">
				        Submit
				        </Button>
				        </div>
				        :
				        <div className="text-center	m-3">
				        <Button variant="secondary" type="submit" id="submitBtn" disabled>
				            Submit
				        </Button>
				        </div>
				    }
				</Form>
				</Card>
				</Col>
			</Row>
		</Container>
		
	)
}