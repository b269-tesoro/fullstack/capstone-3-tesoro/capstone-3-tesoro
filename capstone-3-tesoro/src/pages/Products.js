// importing from packages
import {useState, useEffect, useContext} from "react";
import {Row, /*Col,*/ Container} from "react-bootstrap";
// importing from within the app
import UserContext from "../UserContext"
import ProductCard from "../components/ProductCard"

export default function Products()
{
	// for add to cart function
	const {user} = useContext(UserContext);

	// useState for placing products into cards for rendering
	const [products, setProducts] = useState();
		
	useEffect(()=>
	{
		if(user.isAdmin)
		{
			fetch(`${process.env.REACT_APP_API_URL}/products/all`,
				{
					headers:
					{
					  Authorization: `Bearer ${localStorage.getItem("token")}`
					}
				})
			.then(res => res.json())
			.then(data =>
			{
				// console.log(data)
				setProducts(data.map(product =>
				{
					return (<ProductCard key={product._id} product={product}/>)
				}))
			})
		}
		else
		{
			fetch(`${process.env.REACT_APP_API_URL}/products/`)
			.then(res => res.json())
			.then(data =>
			{
				console.log(data)
				setProducts(data.map(product =>
				{
					return (<ProductCard key={product._id} product={product}/>)
					// return (<ProductCard key={product._id} product={product}/>)
				}))
			})
		}
		
	},[user.isAdmin])	
	console.log({products})
	/*return (
		<>
		{products}
		</>
	)*/


	return (
	    <Container className="justify-content-center">
	      <Row xs={1} sm={2} md={3} lg={4} className="justify-content-center">
	            {products}
	      </Row>
	    </Container>
	  );

}