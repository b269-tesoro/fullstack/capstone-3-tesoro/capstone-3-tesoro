// importing from packages
import {useState, useEffect, useContext} from "react";
import {Navigate} from 'react-router-dom';
import { Container, Row, Col, Card, Form, Button } from 'react-bootstrap';
import Swal from "sweetalert2"

// importing from within the app
import UserContext from "../UserContext"
import ShawpLogo from "../images/ShawpLogo.png";

export default function Login ()
{
	const {user, setUser} = useContext(UserContext);

	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	const retrieveUserDetails = (token) =>
	{
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,
		{
			method: "POST",
			headers:
			{
				Authorization: `Bearer ${token}`
			}
		}).then(res => res.json()).then(data =>
		{
			// console.log(data);

			setUser(
			{
				id: data._id,
		        email: data.email,
		        username: data.username,
		        isAdmin: data.isAdmin
			})
			console.log(user)
		});
	}

	function authenticate (e)
	{
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`,
		{
			method: "POST",
			headers:
			{
				"Content-Type" : "application/json"
			},
			body: JSON.stringify(
			{
				username: username,
				password: password
			})
		}).then(res=>res.json()).then(data =>
		{
			console.log(data);

			if(typeof data.access !== "undefined") {
			    // The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
			    localStorage.setItem('token', data.access);
			    retrieveUserDetails(data.access);

			    Swal.fire({
			        title: "Login Successful!",
			        icon: "success",
			        text: "Happy Shopping!"
			    })
			}
			else
			{
			    Swal.fire({
			        title: "Authentication failed",
			        icon: "error",
			        text: "Please check your login details and try again"
			    })
			}

		})

		setUsername("");
		setPassword("");
	}

	useEffect(() => {
	    if((username !== '' && password !== '')){
	        setIsActive(true)
	    } else {
	        setIsActive(false)
	    }
	}, [username, password])

	return(
	    (user.id !== null) ?
	    <Navigate to="/" />
	    :
	    <Container className="justify-content-center mt-3">
	    	<Row className="justify-content-center">
	    		<Col xs={12} lg={4}>
	    		<Card className="p-3">
	    		<div className="text-center	mb-5">
	    		<Card.Img variant="top" src={ShawpLogo} alt="Shawp Logo" style={{ width: '100px', height: '100px' }}/>
	    		</div>

	    <Form onSubmit={e => authenticate(e)}>
	        <Form.Group controlId="userEmail" className="p-2">
	            <Form.Label>Username or Email Address</Form.Label>
	            <Form.Control 
	                type="text" 
	                placeholder="username or email"
	                value={username}
	                onChange={e => setUsername(e.target.value)}
	                required
	            />
	            
	        </Form.Group>

	        <Form.Group controlId="password" className="p-2">
	            <Form.Label>Password</Form.Label>
	            <Form.Control 
	                type="password" 
	                placeholder="Password"
	                value={password}
	                onChange={e => setPassword(e.target.value)}
	                required
	            />
	        </Form.Group>

	        {   isActive ?
	        		<div className="text-center	m-3">
	            <Button variant="primary" type="submit" id="submitBtn" >
	                Submit
	            </Button>
	            </div>
	            :
	            <div className="text-center	m-3">
	            <Button variant="primary" type="submit" id="submitBtn" disabled>
	                Submit
	            </Button>
	            </div>
	        }
	    </Form>
  		</Card>
  		</Col>
  	</Row>
  </Container>
	)
}
