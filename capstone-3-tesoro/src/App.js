// importing from npm packages
import {useState, useEffect} from "react";
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
// import {useLocation} from 'react-router-dom'
import {Container} from 'react-bootstrap'

// importing from within the app
import "./App.css";
import {UserProvider} from './UserContext';

import AppNavbar from './components/AppNavbar'
import NoNav from './components/NoNav'
import ProductSearchBar from './components/ProductSearchBar'
import ProductView from './components/ProductView'
import ProductUpdate from './components/ProductUpdate'

import Home from './pages/Home'
import Products from './pages/Products'
import ProductsSearch from './pages/ProductsSearch'
import Cart from './pages/Cart'
import Orders from './pages/Orders'
import Profile from './pages/Profile'

import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'

// admin only pages
import Dashboard from './pages/Dashboard'
import DashboardAdd from './pages/DashboardAdd'
import Archives from './pages/Archives'
import OrdersAll from './pages/OrdersAll'
import UsersAll from './pages/UsersAll'






function App() {
  // User Login Status Functionality START

  // initializing user values
  const [user,setUser] = useState(
  {
    id: null,
    email: null,
    username: null,
    isAdmin: null
  });

  // user storage clear function
  const unsetUser = () =>
  {
    localStorage.clear();
  };


  // setting user values
  useEffect( () =>
  {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,
    {
      method: "POST",
      headers:
      {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }

    }).then(res =>res.json()).then(data =>
    {
      // user is logged in
      if(typeof data._id !== "undefined")
      {
        setUser(
        {
          id: data._id,
          email: data.email,
          username: data.username,
          isAdmin: data.isAdmin
        })
      }

      else
      {
        setUser(
        {
          id: null,
          email: null,
          username: null,
          isAdmin: null
        })
      }
    })
  }, [])
  // User Login Status Functionality END

  // For preventing navbar from showing on certain routes
  // const location = useLocation();
  // const hideNavbar = location.pathname === '/login' || location.pathname === '/register';

  return (
    <>
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        {/*{!hideNavbar && <AppNavbar />}*/}
        {/*<AppNavbar />*/}
        {/*<ProductSearchBar />*/}
        <Container>
          <Routes>
          <Route element={<><AppNavbar/><ProductSearchBar /></>}>
            <Route path="/" element={<Home/>} />
            <Route path="/dashboard" element={<Dashboard/>} />
            <Route path="/dashboard/add" element={<DashboardAdd/>} />
            <Route path="/products/archives/all" element={<Archives/>} />
            <Route path="/products" element={<Products/>} />
            <Route path="/cart" element={<Cart/>} />
            <Route path="/orders" element={<Orders/>} />
            <Route path="/profile" element={<Profile/>} />
            <Route path="/orders/all" element={<OrdersAll/>} />
            <Route path="/users/all" element={<UsersAll/>} />
            <Route path="/products/:productId" element={<ProductView/>} />
            <Route path="/products/:productId/update" element={<ProductUpdate/>} />
            <Route path="/products/search" element={<ProductsSearch/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/*" element={<Error/>} />
          </Route>

          <Route element={<NoNav/>}>
            <Route path="/register" element={<Register/>} />
            <Route path="/login" element={<Login/>} />
          </Route>

          </Routes>
        </Container>
      </Router>
    </UserProvider>
    </>
  );
}

export default App;
